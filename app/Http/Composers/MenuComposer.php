<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
class MenuComposer
{
	public function compose(View $view)
	{
		$this->currentLanguage = DB::table('languages')->where([
            'isvisible' => 1,
            'code' =>  LaravelLocalization::getCurrentLocale()
        ])->select('id')->first()->id;

        $data = DB::table('comments as C')
            ->join('comment_translates as CT', 'CT.comment_id', '=', 'C.id')
            ->where(['CT.lang_id' => $this->currentLanguage,'C.isVisible'=>DB::raw(1)])
            ->select('C.id','CT.text')
            ->orderBy('C.id', 'desc')
            ->limit(1)
            ->first();

        $text=$data->text;
        $data_id=$data->id;

        $view->with(['text'=>$text,'data_id'=>$data_id]);
	}



}
