<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    public function index()
    {
        $languages = Helper::languages();
        $lang=$this->currentLanguage;

        $abouts=DB::table('abouts as A')
            ->leftJoin('about_translates as AT', function($join) use ($lang) {
                $join->on('A.id','=','AT.about_id')
                    ->where(['AT.lang_id'=>$lang ]);
            })

            ->select('A.id','A.image','AT.title','AT.lang_id','AT.description')
            ->get();


            return view('admin.about',compact('abouts','languages'));
//        }

    }

    public function edit($id)
    {
        $data =   $abouts=DB::table('abouts as A')
            ->leftJoin('about_translates as AT','A.id','=','AT.about_id')
            ->select('A.id','A.image','AT.title','AT.lang_id','AT.description')
            ->where('A.id', $id)->get();

        return $data;
    }

    public function update(Request $request, $id)
    {

        if ($request->image) {

            $currentImage = About::find($id)->image;

            if ($request->image != $currentImage) {

                $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

                \Image::make($request->image)->save(public_path('user/img/blog/') . $filename);

                $currentPhoto = public_path('user/img/blog/') . $currentImage;

                if (file_exists($currentPhoto)) {
                    @unlink($currentPhoto);
                }

                DB::table('abouts')->where('id', $id)->update(['image' => $filename]);
            }
            foreach ($request->title as $key => $title_name) {

                DB::table('about_translates')
                    ->where([
                        ['about_id', $id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'title' => $title_name,
                        'description' => $request->description[$key],
                    ]);
            }
            return 1;
        }else{

            foreach ($request->title as $key => $title_name) {
                DB::table('about_translates')
                    ->where([
                        ['about_id', $id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'title' => $title_name,
                        'description' => $request->description[$key],
                    ]);
            }
            return 1;
        }

    }

}
