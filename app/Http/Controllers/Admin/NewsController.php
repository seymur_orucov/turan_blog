<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class NewsController extends Controller
{
    public function index()
    {

        $languages = Helper::languages();

        $news = DB::table('news as N')
            ->join('news_translates as NT', 'NT.news_id', '=', 'N.id')
            ->where(['NT.lang_id' => $this->currentLanguage])
            ->select('N.id', 'N.image', 'NT.title', 'NT.description')
            ->orderBy('N.id', 'asc')->get();

        return view('admin.news', compact('languages', 'news'));
    }

    public function store(Request $request) {

        if ($request->image) {

            $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('user/img/blog/') . $filename);

            $id = DB::table('news')->insertGetId(['image' => $filename]);

            foreach ($request->title as $key => $title_name) {

                DB::table('news_translates')->insert([
                    'news_id' => $id,
                    'lang_id' => $request->lang_id[$key],
                    'title' => $title_name,
                    'description' => $request->description[$key]
                ]);
            }
            return $id;
        }
    }

    public function edit($id)
    {
        $data = DB::table('news as N')
            ->join('news_translates as NT', 'N.id', '=', 'NT.news_id')
            ->select('N.id', 'N.image', 'NT.title', 'NT.description')->where('N.id', $id)->get();
        return $data;
    }

    public function update(Request $request, $id)
    {

        if ($request->image) {

            $currentImage = News::find($id)->image;

            if ($request->image != $currentImage) {

                $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

                \Image::make($request->image)->save(public_path('user/img/blog/') . $filename);

                $currentPhoto = public_path('user/img/blog/') . $currentImage;

                if (file_exists($currentPhoto)) {
                    @unlink($currentPhoto);
                }

               DB::table('news')->where('id', $id)->update(['image' => $filename]);
            }
                foreach ($request->title as $key => $title_name) {
                    DB::table('news_translates')
                        ->where([
                            ['news_id', $id],
                            ['lang_id', $request->lang_id[$key]]
                        ])
                        ->update([
                            'title' => $title_name,
                            'description' => $request->description[$key],
                        ]);
                }
                return 1;
            }else{
                foreach ($request->title as $key => $title_name) {
                    DB::table('news_translates')
                        ->where([
                            ['news_id', $id],
                            ['lang_id', $request->lang_id[$key]]
                        ])
                        ->update([
                            'title' => $title_name,
                            'description' => $request->description[$key],
                        ]);
                }
                return 1;
            }

    }

    public function destroy($id)
    {

        $currentImage = News::find($id)->image;

        $currentPhoto = public_path('user/img/blog/') . $currentImage;

        if (file_exists($currentPhoto)) {
            unlink($currentPhoto);
        }

        DB::table('news')->where('id', $id)->delete();
        return response()->json();
    }

    public function open($id)
    {
        $languages = Helper::languages();
        $lang=$this->currentLanguage;

        $news=DB::table('news as N')
            ->leftJoin('news_translates as NT', function($join) use ($lang) {
                $join->on('N.id','=','NT.news_id')
                    ->where(['NT.lang_id'=>$lang ]);
            })
            ->where('N.id', $id)
            ->select('N.id','NT.title')
            ->first();

        if($news){
            $data = DB::table('news as N')
                ->join('news_translates as NT', 'N.id', '=', 'NT.news_id')
                ->select('NT.text')->where('N.id', $id)->get();

            $description=[];
            foreach($data as $key=>$value){
                $description[$key]=$value->text;
            }
            $description = collect($description);

            return view('admin.news_open',compact('description','languages','news'));
        }

    }

    public function save(Request $request, $id)
    {
        $news=DB::table('news')->where('id',$id)->select('id')->first();

        if($news){
            foreach ($request->description as $key => $description_name) {
                DB::table('news_translates')
                    ->where([
                        ['news_id', $news->id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'text' => $description_name
                    ]);
            }
            return 1;
        }
    }

}
