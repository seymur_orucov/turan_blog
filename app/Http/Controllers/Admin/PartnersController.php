<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Partner;

class PartnersController extends Controller
{
    public function index(){

        $languages = Helper::languages();
        $partners = DB::table('partners as P')
            ->select('P.id', 'P.image')
            ->orderBy('P.id', 'asc')->get();

        return view('admin.partners', compact('languages', 'partners'));
    }

    public function store(Request $request){

        if ($request->image) {

            $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('user/img/client/') . $filename);

            $id = DB::table('partners')->insertGetId(['image' => $filename]);

            return $id;
        }

    }

    public function edit($id){
        $data = DB::table('partners')->where('id', $id)->select('id', 'image')->get();
        return $data;
    }

    public function update(Request $request, $id){
        if ($request->image) {

            $currentImage = Partner::find($id)->image;

            if ($request->image != $currentImage) {

                $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

                \Image::make($request->image)->save(public_path('user/img/client/') . $filename);

                $currentPhoto = public_path('user/img/client/') . $currentImage;

                if (file_exists($currentPhoto)) {
                    @unlink($currentPhoto);
                }

                 DB::table('partners')->where('id', $id)->update(['image' => $filename]);

            }

            return 2;

        }

        return 1;


    }

    public function destroy($id){

        $currentImage = Partner::find($id)->image;

        $currentPhoto = public_path('user/img/client/') . $currentImage;

        if (file_exists($currentPhoto)) {
            unlink($currentPhoto);
        }

        DB::table('partners')->where('id', $id)->delete();
        return response()->json();
    }

}
