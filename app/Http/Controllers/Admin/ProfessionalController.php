<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Professional;
use File;


class ProfessionalController extends Controller
{
     public function index(){

        $languages = Helper::languages();
        $icon_array = file_get_contents(public_path('admin/icon-png.json'));
        $icons = json_decode($icon_array);
        $edit_route = route('admin.professionals');
        $save_route = route('admin.professionals.store');

        $professionals = DB::table('professionals as P')
            ->join('professional_translates as PT', 'PT.professional_id', '=', 'P.id')
            ->where(['PT.lang_id' => $this->currentLanguage])
            ->select('P.id', 'P.icon', 'PT.title', 'PT.description')
            ->orderBy('P.id', 'asc')->get();

        return view('admin.professional', compact('languages', 'professionals', 'icons', 'edit_route', 'save_route'));

    }

    public function store(Request $request)
    {
//        dd($request->all());
            if($request->icon) {
                $id = DB::table('professionals')->insertGetId(['icon' => $request->icon]);
                foreach ($request->title as $key => $title_name) {
                    DB::table('professional_translates')->insert([
                        'professional_id' => $id,
                        'lang_id' => $request->lang_id[$key],
                        'title' => $title_name,
                        'description' => $request->description[$key]
                    ]);
                }
                return $id;
        }

    }

    public function edit($id){

        $data = DB::table('professionals as P')
            ->join('professional_translates as PT', 'P.id', '=', 'PT.professional_id')
            ->select('P.id', 'P.icon', 'PT.title', 'PT.description')->where('P.id', $id)->get();
        return $data;

    }

    public function update(Request $request, $id)
    {
               if ($request->icon) {
                    DB::table('professionals')->where('id', $id)->update(['icon' => $request->icon]);
                    foreach ($request->title as $key => $title_name) {
                        DB::table('professional_translates')
                            ->where([
                                ['professional_id', $id],
                                ['lang_id', $request->lang_id[$key]]
                            ])
                            ->update([
                                'title' => $title_name,
                                'description' => $request->description[$key],
                            ]);
                    }
                    return 1;
                } else {
                    foreach ($request->title as $key => $title_name) {
                        DB::table('professional_translates')
                            ->where([
                                ['professional_id', $id],
                                ['lang_id', $request->lang_id[$key]]
                            ])
                            ->update([
                                'title' => $title_name,
                                'description' => $request->description[$key],
                            ]);
                    }
                    return 1;
                }
    }


    public function destroy($id)
    {
        DB::table('professionals')->where('id', $id)->delete();
        return response()->json();
    }


}
