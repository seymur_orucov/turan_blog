<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SliderController extends Controller
{
    public function index()
    {
        $languages = Helper::languages();

        $sliders = DB::table('sliders as S')
            ->join('slider_translates as ST', 'ST.slider_id', '=', 'S.id')
            ->where(['ST.lang_id' => $this->currentLanguage])
            ->select('S.id', 'S.image', 'S.category_id', 'ST.title', 'ST.subtitle','ST.author')
            ->orderBy('S.id', 'asc')->get();

        return view('admin.sliders', compact('languages', 'sliders'));
    }

    public function store(Request $request) {


        if ($request->image) {

            $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('user/img/bg/') . $filename);

            $id = DB::table('sliders')->insertGetId(['image' => $filename,'category_id'=>$request->category_id]);
;
            foreach ($request->title as $key => $title_name) {

                DB::table('slider_translates')->insert([
                    'slider_id' => $id,
                    'lang_id' => $request->lang_id[$key],
                    'title' => $title_name,
                    'subtitle' => $request->subtitle[$key],
                    'author' => $request->author[$key]
                ]);
            }
            return $id;
        }
    }

    public function edit($id)
    {
        $data = DB::table('sliders as S')
            ->join('slider_translates as ST', 'S.id', '=', 'ST.slider_id')
            ->select('S.id', 'S.image','S.category_id', 'ST.title', 'ST.subtitle','ST.author')->where('S.id', $id)->get();
        return $data;
    }

    public function update(Request $request, $id)
    {
//        dd($request->all());
        if ($request->image) {

            $currentImage = Slider::find($id)->image;

            if ($request->image != $currentImage) {

                $filename = 'img' . '-' . time() . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

                \Image::make($request->image)->save(public_path('user/img/bg/') . $filename);

                $currentPhoto = public_path('user/img/bg/') . $currentImage;

                if (file_exists($currentPhoto)) {
                    @unlink($currentPhoto);
                }

                DB::table('sliders')->where('id', $id)->update(['image' => $filename,'category_id'=>$request->category_id]);
            }
                foreach ($request->title as $key => $title_name) {
                    DB::table('slider_translates')
                        ->where([
                            ['slider_id', $id],
                            ['lang_id', $request->lang_id[$key]]
                        ])
                        ->update([
                            'title' => $title_name,
                            'subtitle' => $request->subtitle[$key],
                            'author' => $request->author[$key]
                        ]);
                }
                return 2;
            }else{
            DB::table('sliders')->where('id', $id)->update(['category_id'=>$request->category_id]);

            foreach ($request->title as $key => $title_name) {
                    DB::table('slider_translates')
                        ->where([
                            ['slider_id', $id],
                            ['lang_id', $request->lang_id[$key]]
                        ])
                        ->update([
                            'title' => $title_name,
                            'subtitle' => $request->subtitle[$key],
                            'author' => $request->author[$key]
                        ]);
                }
                return 1;
            }

    }

    public function destroy($id)
    {

        $currentImage = Slider::find($id)->image;

        $currentPhoto = public_path('user/img/bg/') . $currentImage;

        if (file_exists($currentPhoto)) {
            unlink($currentPhoto);
        }

        DB::table('sliders')->where('id', $id)->delete();
        return response()->json();
    }

}
