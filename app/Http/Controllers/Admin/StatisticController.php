<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    public function open($id)
    {
        $languages = Helper::languages();
        $lang=$this->currentLanguage;

        $project=DB::table('projects as P')
            ->leftJoin('project_translates as PT', function($join) use ($lang) {
                $join->on('P.id','=','PT.project_id')
                    ->where(['PT.lang_id'=>$lang ]);
            })
            ->where('P.id', $id)
            ->select('P.id','PT.title')
            ->first();

        if($project){
            $data = DB::table('projects as P')
                ->join('project_translates as PT', 'P.id', '=', 'PT.project_id')
                ->select('PT.text')->where('P.id', $id)->get();

            $description=[];
            foreach($data as $key=>$value){
                $description[$key]=$value->text;
            }
            $description = collect($description);

            return view('admin.project_open',compact('description','languages','project'));
        }

    }

    public function save(Request $request, $id)
    {
        $project=DB::table('projects')->where('id',$id)->select('id')->first();

        if($project){
            foreach ($request->description as $key => $description_name) {
                DB::table('project_translates')
                    ->where([
                        ['project_id', $project->id],
                        ['lang_id', $request->lang_id[$key]]
                    ])
                    ->update([
                        'text' => $description_name
                    ]);
            }
            return 1;
        }
    }
}
