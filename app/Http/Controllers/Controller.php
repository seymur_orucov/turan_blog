<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $languages;
    protected $currentLanguage;

    function __construct()
    {
        $this->languages = DB::table('languages')->where('isvisible', 1)->get();

//        dd(LaravelLocalization::getCurrentLocale());
        $this->currentLanguage = DB::table('languages')->where([
            'isvisible' => 1,
            'code' =>  LaravelLocalization::getCurrentLocale()
        ])->select('id')->first()->id;

    }



}
