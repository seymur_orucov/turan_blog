<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){

        $search_data=$request->search_data;
        $lang=$this->currentLanguage;

        $query1=DB::table('projects as P')
            ->leftJoin('project_translates as PT', function($join) use ($lang) {
                $join->on('P.id','=','PT.project_id')
                    ->where(['PT.lang_id'=>$lang ]);
            })->select('P.id','PT.title','PT.description',DB::raw("'project-description' as name"));

        if (!empty($search_data)) {
            $query1->where(function($query) use ($search_data){
                $query->where('PT.title','LIKE','%'.$search_data.'%')
                    ->orWhere('PT.description','LIKE','%'.$search_data.'%');
            });
        }
        $data1=$query1->get();

        $query2=DB::table('news as N')
            ->leftJoin('news_translates as NT', function($join) use ($lang) {
                $join->on('N.id','=','NT.news_id')
                    ->where(['NT.lang_id'=>$lang ]);
            })->select('N.id','NT.title','NT.description',DB::raw("'news-description' as name"));

        if (!empty($query2)) {
            $query2->where(function($query2) use ($search_data){
                $query2->where('NT.title','LIKE','%'.$search_data.'%')
                    ->orWhere('NT.description','LIKE','%'.$search_data.'%');
            });
        }
        $data2=$query2->get();

        $query3 = DB::table('comments as C')
            ->join('comment_translates as CT', 'CT.comment_id', '=', 'C.id')
            ->where(['CT.lang_id' => $this->currentLanguage,'C.isVisible'=>DB::raw(1)])
            ->select('C.id','CT.title','CT.text',DB::raw("'comments' as name"));

        if (!empty($query3)) {
            $query3->where(function($query3) use ($search_data){
                $query3->where('CT.title','LIKE','%'.$search_data.'%')
                    ->orWhere('CT.text','LIKE','%'.$search_data.'%');
            });
        }

        $data3=$query3->get();

        if($query3->count()>0){
            if($query2->count()>0){
                if($query1->count()>0) {
                    $merged1 = $data2->merge($data3);
                    $merged=$data1->merge($merged1);
                    $result = $merged->all();
                }else{
                    $merged = $data2->merge($data3);
                    $result = $merged->all();
                }
            }else{
                if($query1->count()>0) {
                    $merged = $data1->merge($data3);
                    $result = $merged->all();
                }else{
                    $result=$query3->get();
                }
            }

        }else{
            if($query2->count()>0){
                $merged=$data1->merge($data2);
                $result=$merged->all();
            }else{
                $result=$query1->get();
            }

        }

        $count=count($result);

        return ['data' => $result, 'count' => $count];
    }
}
