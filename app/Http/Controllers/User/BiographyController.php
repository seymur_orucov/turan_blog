<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BiographyController extends Controller
{
    public function index(){

        $lang=$this->currentLanguage;
        $sliders = DB::table('sliders as S')
            ->join('slider_translates as ST', 'ST.slider_id', '=', 'S.id')
            ->where(['ST.lang_id' => $lang,'S.category_id'=>2])
            ->select('S.id', 'S.image',  'ST.title', 'ST.subtitle','ST.author')
            ->orderBy('S.id', 'asc')->get();

        $bio=DB::table('biography_translates')
            ->where('biography_translates.lang_id',$this->currentLanguage)
            ->select('biography_translates.id','biography_translates.text')
            ->first();
//        dd($bio);
        return view('user.biography',compact('bio','sliders'));
    }
}
