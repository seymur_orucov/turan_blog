<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function index($id=null){

        if(is_null($id)){
            $data = DB::table('comments as C')
                ->join('comment_translates as CT', 'CT.comment_id', '=', 'C.id')
                ->where(['CT.lang_id' => $this->currentLanguage,'C.isVisible'=>DB::raw(1)])
                ->select('C.id','CT.text')
                ->orderBy('C.id', 'desc')
                ->limit(1)
                ->first();

            $text=$data->text;
            $data_id=$data->id;

        }else{
            $otherData = DB::table('comments as C')
                ->join('comment_translates as CT', 'CT.comment_id', '=', 'C.id')
                ->where(['CT.lang_id' => $this->currentLanguage,'C.isVisible'=>DB::raw(1),'C.id'=>$id])
                ->select('C.id','CT.text')
                ->first();
            $text=$otherData->text;
            $data_id=$otherData->id;
        }

        $comments = DB::table('comments as C')
            ->join('comment_translates as CT', 'CT.comment_id', '=', 'C.id')
            ->where([['CT.lang_id',$this->currentLanguage],['C.isVisible',DB::raw(1)],['C.id','<>',$data_id]])
            ->select('C.id','C.created_at','CT.title','CT.text')
            ->orderBy('C.id', 'desc')
            ->limit(3)
            ->get();

      return view('user.comments',compact('comments','text','data_id'));
    }

    public function vote_send(Request $request){

        $vote_id=1;
        $request_vote = $request->vote;
        if ($request_vote == 1){

//            $results=DB::table('vote_results')->where('vote_id',$vote_id)->get();
//            if(count($results)==0) {
            if(is_null($request->checkedVote)){

                $results = DB::table('votes')->where('id', $vote_id)->first();
                $question = $results->question;
                $voteOptions = DB::table('vote_options')->where('vote_id', $vote_id)->get();
                $html = "<h3>" . $question . "</h3>
                      <form >
                        <input type=\"checkbox\" class=\" selectall check-input\" id=\"category0\" value=\"all\" name=\"vote\">
                        <label for=\"category0\">Hər Biri</label><br>";

                foreach ($voteOptions as $voteOption) {
                    $voteId = $voteOption->id;
                    $voteName = $voteOption->name;

                    $html .= "<input type=\"checkbox\" class=\"individual check-input\" id=\"category" . $voteId . "\" value=" . $voteId . " name=\"vote\">
                         <label for=\"category" . $voteId . "\">" . $voteName . "</label><br>";
                }
                $html .= "<button type=\"button\" class=\"submit-btn saveVote\">Göndər</button>
                        </form>";
//            }
            }else{

                $results=DB::table('vote_results')->where('vote_id',$vote_id)
                    ->select(DB::raw('COUNT(*) as allCounts'))
                    ->where('vote_id',$vote_id)
                    ->first();
                $allCount=$results->allCounts;

                $voteResult=DB::table('vote_options')
                    ->leftJoin('vote_results','vote_results.vote_option_id','=','vote_options.id')
                    ->where('vote_options.vote_id',$vote_id)
                    ->select('vote_options.id','vote_options.name',DB::raw('COUNT(vote_results.vote_option_id) as votes'))
                    ->groupBy('vote_options.id')
                    ->get();

                $html="<div class=\"poll-container\">
                                    <div id=\"poll\">";

                foreach($voteResult as $result) {
                    $voteId=$result->id;
                    $voteName=$result->name;
                    $votes=$result->votes;

                    // Find percentage
                    $percentage = ($votes / $allCount) * 100;

                    $html.="<div class=\"pol_option\">
                            <div class=\"poll-optionname-".$voteId."\">".$voteName."</div>
                            <div class=\"poll-votes\">" . round($percentage, 2) . " %</div>
                          </div>";

                }
                $html.="</div>
                      </div>";
            }
//            dd($html);
            echo $html;
//            exit;
        } if ($request_vote == 2) {
            $vote_option=$request->vote_option;
            $response=0;
//            $results=DB::table('vote_results')->where('vote_id',$vote_id)->get();
//            if(count($results)==0){
            $result_id=[];
            foreach($vote_option as $vote_option_id){
                $id=DB::table('vote_results')
                    ->insertGetId([
                        'vote_id'=>$vote_id,
                        'vote_option_id'=>$vote_option_id
                    ]);
                array_push($result_id,$id);
            }
                if(count($result_id)>0){
                    $response=1;
                }

//            }
            echo $response;
//            exit;
        }
    }

}
