<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function index(){

        $lang=$this->currentLanguage;

        $contacts=DB::table('contacts as C')
            ->leftJoin('contact_translates as CT', function($join) use ($lang) {
                $join->on('CT.contact_id', '=', 'C.id')
                    ->where(['CT.lang_id'=>$lang ]);
            })
            ->orderBy('C.id', 'asc')->get();

        return view('user.contact',compact('contacts'));
    }

    public function send_mail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email'=>'required',
            'subject'=>'required',
            'message'=>'required'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        Mail::send(new ContactMail($request));
//       return  Session::flash('message', 'This is a message!');
       return redirect('contact')->with('status', 'Mesajınız göndərildi');

    }
}
