<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){

        $partners=DB::table('partners')->get();

        $lang=$this->currentLanguage;
//        dd($lang);

        $news=DB::table('news as N')
            ->leftJoin('news_translates as NT', function($join) use ($lang) {
                $join->on('N.id','=','NT.news_id')
                    ->where(['NT.lang_id'=>$lang ]);
            })
            ->get();

        $projects=DB::table('projects as P')
            ->leftJoin('project_translates as PT', function($join) use ($lang) {
                $join->on('P.id','=','PT.project_id')
                    ->where(['PT.lang_id'=>$lang ]);
            })
            ->get();

        $professionals= DB::table('professionals as P')
            ->leftJoin('professional_translates as PT', function($join) use ($lang) {
                $join->on('P.id','=','PT.professional_id')
                    ->where(['PT.lang_id'=>$lang ]);
            })
            ->get();

        $abouts=DB::table('abouts as A')
            ->leftJoin('about_translates as AT', function($join) use ($lang) {
                $join->on('A.id','=','AT.about_id')
                    ->where(['AT.lang_id'=>$lang ]);
            })
            ->select('A.id','A.image','AT.title','AT.lang_id','AT.description')
            ->first();

        $sliders = DB::table('sliders as S')
            ->join('slider_translates as ST', 'ST.slider_id', '=', 'S.id')
            ->where(['ST.lang_id' => $lang,'S.category_id'=>1])
            ->select('S.id', 'S.image',  'ST.title', 'ST.subtitle','ST.author')
            ->orderBy('S.id', 'asc')->get();

        return view('user.index',compact('partners','news','projects',
            'professionals','abouts','sliders'));
    }

}
