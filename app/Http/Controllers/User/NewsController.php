<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
     public function index(){

        $lang=$this->currentLanguage;

        $news=DB::table('news as N')
            ->leftJoin('news_translates as NT', function($join) use ($lang) {
                $join->on('N.id','=','NT.news_id')
                    ->where(['NT.lang_id'=>$lang ]);
            })
            ->select('N.*','NT.title','NT.description')
            ->paginate(4);

        return view('user.news',compact('news'));
    }

    public function show($id){

        $lang=$this->currentLanguage;

        $project=DB::table('news')->where('id',$id)->select('id')->first();

        if($project){
            $news=DB::table('news as N')
                ->leftJoin('news_translates as NT', function($join) use ($lang) {
                    $join->on('N.id','=','NT.news_id')
                        ->where(['NT.lang_id'=>$lang ]);
                })
                ->where('N.id',$project->id)
                ->select('NT.text')
                ->first();

            return view('user.news-description',compact('news'));
        }

    }
}
