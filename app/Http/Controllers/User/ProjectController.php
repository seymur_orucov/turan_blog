<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    public function index(){

        $lang=$this->currentLanguage;

        $projects=DB::table('projects as P')
            ->leftJoin('project_translates as PT', function($join) use ($lang) {
                $join->on('P.id','=','PT.project_id')
                    ->where(['PT.lang_id'=>$lang ]);
            })->select('P.*','PT.title','PT.description')->paginate(4);

        return view('user.projects',compact('projects'));
    }

    public function show($id){

        $lang=$this->currentLanguage;

        $project=DB::table('projects')->where('id',$id)->select('id')->first();

        if($project){
            $projects=DB::table('projects as P')
                ->leftJoin('project_translates as PT', function($join) use ($lang) {
                    $join->on('P.id','=','PT.project_id')
                        ->where(['PT.lang_id'=>$lang ]);
                })
                ->where('P.id',$project->id)
                ->select('PT.text')
                ->first();

            return view('user.project-description',compact('projects'));
        }

    }
}
