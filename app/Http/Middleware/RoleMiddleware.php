<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next,$role = null)
    {
        
            if ($user = Sentinel::getUser()) {
                if ($user->inRole($role) ||  $user->inRole('admin') ) {
                     return $next($request);
                 }else{
                    return redirect('admin/login')->with('message', 'Access denied');
                 }

            }else{
                return redirect('admin/login')->with('message', 'Access denied');
            }

        
     
    }
}
