
    $( document ).ready(function() {

        var table=$('.datatable-comments').DataTable( {
            "bFilter": false,
            "order": [[ 0, "asc" ]]
        });

        function iconFormat(icon) {
            var originalOption = icon.element;
            if (!icon.id) {
                return icon.text;
            }
            var $icon = '<i class="fa ' + $(icon.element).data('icon') + '"></i>' + icon.text;

            return $icon;
        }

        $('.select-icons').select2({
            templateResult: iconFormat,
            minimumResultsForSearch: Infinity,
            templateSelection: iconFormat,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $(document).ready(function() {
            $('.summernote').summernote();
            $(".desc .note-editable").addClass('description-editor');
        });

        $('.datatable-comments').on("click",'.editComment',function(){

            $(".in").css("display","none");
            $("#editProfessionalModal").modal('show');
            var id = $(this).closest('tr').data('id');
            var route = $(this).closest('table').data('route');

            var url=route.concat("/edit/"+id);
            $(".edit_url").val(url);

            $.ajax({
                url:url,
                method:"GET",
                success:function(response){

                    $.each(response,function (key,value) {

                        if(value.title){
                            $(`.tab${key} .title`).val(value.title);
                            $(`.tab${key} #title`).show();
                        }

                        if(value.text){
                            $(`.tab${key} .description-editor`).html(value.text);
                            $(`.tab${key} #description`).show();
                        }
                    })
                }
            });

        });

        $(".updateComment").on("click",function(){

            $('.updateComment').prop('disabled', true);
            var url=$(".edit_url").val();

            var title=$('input[name="title[]"]').map(function(){
                return $(this).val();
            }).get();

            var lang_id=$('input[name="lang_id[]"]').map(function(){
                return $(this).val();
            }).get();

            var description=$(this).closest('form').find('.description-editor').map(function(){
                if($(this).html() !== "<p><br></p>"){
                    return $(this).html();
                }else{
                    return $(this).text();
                }
            }).get();

            $.ajax({
                url: url,
                method: "POST",
                data:{
                    title,
                    description,
                    lang_id,
                    '_token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    if (response ){
                        $("#editModal").modal('hide');
                        swal("Update Successfully !"," ",
                            "success" ).then(function(){
                                location.reload();
                            }
                        );
                    }

                }
            });
        });

        $(".saveComment").on("click",function(){

            // $('.saveProfessional').prop('disabled', true);
            var url = $(".datatable-comments").data('route');

            var description=$(this).closest('form').find('.description-editor').map(function(item){
                if($(this).html() !== "<br>"){
                    return $(this).html();
                }else{
                    return $(this).text();
                }
            }).get();

            var title=$('input[name="titles[]"]').map(function(){
                return $(this).val();
            }).get();

            var lang_id=$('input[name="lang_id[]"]').map(function(){
                return $(this).val();
            }).get();

            var visible=$("#other").val();

            $.ajax({
                url: url,
                method: "POST",
                data:{
                    title,
                    description,
                    visible,
                    lang_id,
                    '_token': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response) {
                    console.log(response);
                    if(response.length >0){
                        swal("Save successfully !","You clicked the button!",
                            "success" ).then(function(){
                                location.reload();
                            }
                        );
                    }

                    // $("#editModal").modal('hide');

                }
            });
        });

        $(".datatable-comments").on("click",".deleteComment",function() {
            var id = $(this).closest('tr').data('id');
            var route = $('table').data('route');
            var url = route.concat("/delete/" + id);

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this ...",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url:url,
                            method:"GET",
                            success:function(){
                                swal(" This comment has been deleted!", {
                                    icon: "success",
                                }).then(function() {
                                    location.reload();
                                });
                            }
                        });
                    } else {
                        swal("This comment is safe!");
                    }
                });
        });

        if($("#switch").is(':checked')){
            var value=  $("#other").val();
            $("#switch").on("click",function() {
                value = !value;
                var i = value ? 1 : 0;
                $("#other").val(i);
            });
        }


    });
