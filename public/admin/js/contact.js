$( document ).ready(function() {

    var table=$('.datatable-contacts').DataTable( {
        "bFilter": false,

    });



    $('.datatable-contacts').on("click",'.editContact',function(){

        $("#editContactModal").modal('show');
        var id = $(this).closest('tr').data('id');
        var route = $(this).closest('table').data('route');

        var url=route.concat("/edit/"+id);
        $(".edit_url").val(url);

        $.ajax({
            url:url,
            method:"GET",
            success:function(response){

                if(response[0]['phone']){
                    var phone=response[0]['phone'];
                    $("#phone").val(phone);
                }

                $.each(response,function (key,value) {
                    if(value.country){
                        $(`.tab${key} .country`).val(value.country);
                        $(`.tab${key} #country`).show();
                    }
                })


            }
        });

    });

    $(".updateContact").on("click",function(){

        $('.updateContact').prop('disabled', true);
        var url=$(".edit_url").val();

        var lang_id=$('input[name="lang_id[]"]').map(function(){
            return $(this).val();
        }).get();

        var country=$('input[name="country[]"]').map(function(){
            return $(this).val();
        }).get();

        var phone=$("#phone").val();

        $.ajax({
            url: url,
            method: "POST",
            data:{
                country,
                phone,
                lang_id,
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response ){
                    $("#editModal").modal('hide');
                    swal("Update Successfully !"," ",
                        "success" ).then(function(){
                            location.reload();
                        }
                    );
                }

            }
        });
    });

    $(".saveContact").on("click",function(){

        var url = $(".datatable-contacts").data('route');

        var country=$('input[name="countries[]"]').map(function(){
            return $(this).val();
        }).get();

        var lang_id=$('input[name="lang_id[]"]').map(function(){
            return $(this).val();
        }).get();

        var phone=$(".phone").val();

        $.ajax({
            url: url,
            method: "POST",
            data:{
                country,
                phone,
                lang_id,
                '_token': $('meta[name="csrf-token"]').attr('content')
            },

            success: function (response) {
                console.log(response);
                if(response.length >0){
                    swal("Save successfully !","You clicked the button!",
                        "success" ).then(function(){
                            location.reload();
                        }
                    );
                }

            }
        });
    });

    $(".datatable-contacts").on("click",".deleteContact",function() {

        var id = $(this).closest('tr').data('id');
        var route = $('table').data('route');
        var url = route.concat("/delete/" + id);

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this ...",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url:url,
                        method:"GET",
                        success:function(){
                            swal(" This contact has been deleted!", {
                                icon: "success",
                            }).then(function() {
                                location.reload();
                            });
                        }
                    });
                } else {
                    swal("This contact is safe!");
                }
            });
    });

});

// Modal template
var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
    '  <div class="modal-content">\n' +
    '    <div class="modal-header align-items-center">\n' +
    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '      <div class="floating-buttons btn-group"></div>\n' +
    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n';

// Buttons inside zoom modal
var previewZoomButtonClasses = {
    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
    fullscreen: 'btn btn-light btn-icon btn-sm',
    borderless: 'btn btn-light btn-icon btn-sm',
    close: 'btn btn-light btn-icon btn-sm'
};

// Icons inside zoom modal classes
var previewZoomButtonIcons = {
    prev: '<i class="icon-arrow-left32"></i>',
    next: '<i class="icon-arrow-right32"></i>',
    toggleheader: '<i class="icon-menu-open"></i>',
    fullscreen: '<i class="icon-screen-full"></i>',
    borderless: '<i class="icon-alignment-unalign"></i>',
    close: '<i class="icon-cross2 font-size-base"></i>'
};

