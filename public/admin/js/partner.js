$( document ).ready(function() {

    var table=$('.datatable-partners').DataTable( {
        "bFilter": false,
    });

    $('.datatable-partners').on("click",'.editPartner',function(){

        $("#editModal").modal('show');
        var id = $(this).closest('tr').data('id');
        var route = $(this).closest('table').data('route');

        var url=route.concat("/edit/"+id);
        $(".edit_url").val(url);

        $.ajax({
            url:url,
            method:"GET",
            success:function(response){

                $('.file-input-ajaxx').fileinput({
                    browseLabel: 'Select',
                    uploadUrl: "http://localhost", // server upload action
                    uploadAsync: true,
                    maxFileCount: 5,
                    initialPreview: [
                        '../../images/'+response[0]['image']


                    ],
                    initialPreviewConfig:[
                        {caption:response[0]['image'],key:1,url:'{$url}'}
                    ],
                    initialPreviewAsData:true,
                    browseIcon: '<i class="icon-image2 mr-2"></i>',
                    uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                    removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                    fileActionSettings: {
                        removeIcon: '<i class="icon-bin"></i>',
                        uploadIcon: '<i class="icon-upload"></i>',
                        uploadClass: '',
                        zoomIcon: '<i class="icon-zoomin3"></i>',
                        zoomClass: '',
                        indicatorNew: '<i class="icon-file-plus text-success"></i>',
                        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                        indicatorError: '<i class="icon-cross2 text-danger"></i>',
                        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
                    },
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate
                    },
                    initialCaption:   response[0]['image'],
                    overwriteInitial:true,
                    previewZoomButtonClasses: previewZoomButtonClasses,
                    previewZoomButtonIcons: previewZoomButtonIcons

                });


            }
        });

    });

    $(".updatePartner").on("click",function(){

        $('.updatePartner').prop('disabled', true);
        var url=$(".edit_url").val();
        var image=$("#preview").attr("src");

        $.ajax({
            url: url,
            method: "POST",
            data:{
                image,
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response ){
                    $("#editModal").modal('hide');
                    swal("Update Successfully !"," ",
                        "success" ).then(function(){
                            location.reload();
                        }
                    );
                }

            }
        });
    });

    $('#image').on('change', function () {
        var reader = new FileReader();
        reader.onload = function () {
            $('#preview').attr('src', reader.result);
        };
        reader.readAsDataURL(this.files[0]);

    });

    $('#images').on('change', function () {
        var reader = new FileReader();
        reader.onload = function () {
            $('#previews').attr('src', reader.result);
        };
        reader.readAsDataURL(this.files[0]);

    });

    $(".savePartner").on("click",function(){

        // $('.saveSection').prop('disabled', true);
        var url = $(".datatable-partners").data('route');

        var image=$("#previews").attr("src");


        $.ajax({
            url: url,
            method: "POST",
            data:{
                image,
                '_token': $('meta[name="csrf-token"]').attr('content')
            },

            success: function (response) {
                console.log(response);
                if(response.length >0){
                    swal("Save successfully !","You clicked the button!",
                        "success" ).then(function(){
                            location.reload();
                        }
                    );
                }

                // $("#editModal").modal('hide');

            }
        });
    });

    $(".datatable-partners").on("click",".deletePartner",function() {

        var id = $(this).closest('tr').data('id');
        var route = $('table').data('route');
        var url = route.concat("/delete/" + id);

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this ...",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url:url,
                        method:"GET",
                        success:function(){
                            swal(" This partner has been deleted!", {
                                icon: "success",
                            }).then(function() {
                                location.reload();
                            });
                        }
                    });
                } else {
                    swal("This partner is safe!");
                }
            });
    });


});

// Modal template
var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
    '  <div class="modal-content">\n' +
    '    <div class="modal-header align-items-center">\n' +
    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '      <div class="floating-buttons btn-group"></div>\n' +
    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n';

// Buttons inside zoom modal
var previewZoomButtonClasses = {
    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
    fullscreen: 'btn btn-light btn-icon btn-sm',
    borderless: 'btn btn-light btn-icon btn-sm',
    close: 'btn btn-light btn-icon btn-sm'
};

// Icons inside zoom modal classes
var previewZoomButtonIcons = {
    prev: '<i class="icon-arrow-left32"></i>',
    next: '<i class="icon-arrow-right32"></i>',
    toggleheader: '<i class="icon-menu-open"></i>',
    fullscreen: '<i class="icon-screen-full"></i>',
    borderless: '<i class="icon-alignment-unalign"></i>',
    close: '<i class="icon-cross2 font-size-base"></i>'
};
