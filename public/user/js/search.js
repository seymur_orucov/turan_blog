
 $(document).ready(function(){


$('.searchHome').on('click', function (e){
    e.preventDefault();
    $('.search__results').html('');
    var search_data=$("input[name='search_data']").val();
    var url=$(this).data('url');
    var href=$(location).attr('href');
    var index=href.lastIndexOf("/");
    var data_link=href.substring(0,index+1);


    $.ajax({
      method:"POST",
      url:url,
      data:{
        search_data
      },
      success:function(response){
        console.log(response)
        data_count=response['count'];

        data=response['data'];
        console.log(data);
        pagination_data=[];
        size=3;

        if(data_count>0){
          $('.search__result-text').html('  <b>Axtarış nəticələri : '+ data_count+'</b>');

          if(data_count %size ==0){
            n=data_count/size;
          }else{
            n=parseInt(data_count/size) + 1;
          }
          $('#search__pagination').html('');
          $('.pagination-nav').css('display','none');

          for(i=0;i<n;i++){
            $('#search__pagination').append(`<li><a class="active" href="#page-${i+1}">
	      			${i+1}</a></li>`);
          }

          // slice();

          showPage=function(page){

            var search__results = $('.search__results');
            var html = '';
            search__results.html('');
            var href="";
            for(var i = size * (page-1); i <size * page; i++){

              if(data[i]){

                     console.log(href)
                html +=`<div class="search__result">
                        <a class="search__result-title" href="${data_link+data[i].name}/${data[i].id}">${data[i].title}</a>
                        <div class="search__result-description" data-maxlength="150">${data[i].description}</div>
                       </div>`;

              }

            }
            search__results.append(html);

            // slice();

          }

          showPage(1);

          $("#search__pagination li a").click(function(){
            $("#search__pagination li a").removeClass('current')
            $(this).addClass("current");
            showPage(parseInt($(this).text()))

          })

        } else{
          $('.search__result-text').html('<b style="font-size: 17px;color: #232222;;">Search result: 0</b>');

        }



      },

      error:function(){

      }
    })


  }
)





});
