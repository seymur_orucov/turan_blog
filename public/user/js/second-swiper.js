var swiper1 = new Swiper('.s1', {
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

});




/////////////////////////


var swiper2 = new Swiper('.s2', {
    slidesPerView: 3,
    spaceBetween: 30,
    freeMode: true,
    pagination: {
        el: '.swiper-pagination2',
        clickable: true,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints:{
        0: {
            slidesPerView: 1,
        },

        560: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 2,
        },

        992: {
            slidesPerView: 3,
        }
    }
});


////////////////////////////////





var swiper2 = new Swiper('.s3', {
    slidesPerView: 3,
    spaceBetween: 30,
    freeMode: true,
    pagination: {
        el: '.swiper-pagination3',
        clickable: true,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints:{
        0: {
            slidesPerView: 1,
        },

        560: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 2,
        },

        992: {
            slidesPerView: 3,
        }
    }
});