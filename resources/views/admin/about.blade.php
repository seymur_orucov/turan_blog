@extends('layouts.admin')
@section('content')
    <!-- Page content -->
    <div class="page-content">

    @include('admin.partials.sidebar')

    <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - About Us</h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="{{url('/')}}/admin/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="{{route('admin.about')}}" class="breadcrumb-item">About Us</a>

                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <div class="card">

                    <div class="container-fluid mt-3">
                        <table class="table datatable-abouts" data-route="{{route('admin.about')}}">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Actions</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($abouts as $about)
                                <tr data-id="{{$about->id}}">
                                    <td>{{substr($about->title,0,100)}}{{strlen($about->title)>100 ? "..." : ""}}</td>
                                    <td> {{substr($about->description,0,50)}}{{strlen($about->description)>50 ? "..." : ""}} </td>
                                    <td class="text-center">
                                        <a href="{{url('/')}}/user/img/blog/{{$about->image}}" data-popup="lightbox">
                                            <img src="{{url('/')}}/user/img/blog/{{$about->image}}" alt="" class="img-preview rounded" style="max-weight:100px;">
                                        </a>
                                    </td>

                                    <td class="text-center">
                                        <div style="text-align: center;">
                                            <button class=" btn  btn-sm btn-dark btn-rounded editAbout mr-2"><i class="icon-pencil7 mr-2"></i>Edit</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
        <!-- Edit Modal -->
        <div id="editAboutModal" class="modal fade" tabindex="-1">

            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title">Edit modal</h5>
                        <input type="hidden" class="edit_url">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row">

                            <div class="card-body ">
                                <form>
                                    {{csrf_field()}}


                                    <ul class="nav nav-justified nav-tabs mb-0">
                                        <li class="nav-item"><a href="#borderedd-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                        @foreach($languages as $language)
                                            <li class="nav-item"><a href="#tabb_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                        <div class="tab-pane fade show active m-3" id="borderedd-tab1">
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label font-weight-semibold">Image upload:</label>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-10">
                                                    <input type="file" class="file-input-ajax"  name="image" id="image" data-fouc>
                                                    <img src="" id="preview" alt="" style="display:none;">
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($languages as $key=>$language)
                                            <input type="hidden" name="langs_id[]" class="table-input" value="{{$language->id}}">
                                            <div class="tab-pane fade m-3 tab{{$key}}" id="tabb_{{$language->id}}">

                                                <div class="form-group row in" id="title">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold" >Title ({{$language->code}})</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" name="title[]" placeholder="Title" class="form-control title" >
                                                    </div>
                                                </div>

                                                <div class="form-group row in" id="description">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold">Description ({{$language->code}})</label>
                                                    <div class="col-lg-10 desc" >
                                                        <input type="text" name="description[]" placeholder="Description" class="summernote form-control ">
                                                    </div>
                                                </div>

                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="text-right mt-3">
                                        <a href="#" type="button" class="btn bg-warning updateAbout">Save changes</a>
                                    </div>
                                </form>
                            </div>


                            <!-- /bordered tab content -->

                        </div>


                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /page content -->
@endsection

@section('js')
    <script src="{{asset('admin/js/about.js')}}"></script>
@endsection
