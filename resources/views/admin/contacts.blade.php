@extends('layouts.admin')
@section('content')
    <!-- Page content -->
    <div class="page-content">

    @include('admin.partials.sidebar')

    <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Contacts</h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="{{url('/')}}/admin/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="{{route('admin.contacts')}}" class="breadcrumb-item">Contacts</a>
{{--                            <a href="{{route('admin.projects.open',$project->id)}}" class="breadcrumb-item">{{$project->title}}</a>--}}
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <div class="card">
                    <div class="card-header bg-white header-elements-inline">
                        <h5 class="card-title">Contact List</h5>
                        <div class="header-elements">
                            <a  class="btn  btn-sm btn-dark" data-toggle="collapse"
                                href="#section-collapsed" >Add Contact </a>
                        </div>
                    </div>
                    <div class="collapse" id="section-collapsed">
                         <div class="col-md-10 offset-md-1 mt-2">
                        <div class="card-body m-2">
                            <form  enctype="multipart/form-data">
                                {{csrf_field()}}
                                <ul class="nav nav-justified nav-tabs mb-0">
                                    <li class="nav-item"><a href="#bordered-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                    @foreach($languages as $language)
                                        <li class="nav-item"><a href="#tab_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                    @endforeach
                                </ul>

                                <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                    <div class="tab-pane fade show active m-3" id="bordered-tab1">
                                        <div class="form-group row">
                                            <label class="col-lg-2 col-form-label font-weight-semibold">Phone</label>
                                            <div class="col-lg-10">
                                                <input type="text" name="phone" class="form-control phone">
                                            </div>
                                        </div>
                                    </div>

                                    @foreach($languages as $language)
                                        <input type="hidden" name="lang_id[]" class="table-input" value="{{$language->id}}">
                                        <div class="tab-pane fade m-3" id="tab_{{$language->id}}">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label font-weight-semibold" >Country ({{$language->code}})</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="countries[]" class="form-control">
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>

                                <div class="text-right mt-3">
                                    <button type="button" class="btn btn-success saveContact"> <span> Create  </span><i class="icon-paperplane ml-2"></i></button>
                                </div>

                            </form>
                        </div>
                    </div>
                    </div>
                    <br>
                    <div class="container-fluid mt-3">
                        <table class="table datatable-contacts" data-route="{{route('admin.contacts')}}">
                            <thead>
                            <tr>
                                <th class="text-center">Country</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contacts as $contact)
                                <tr data-id="{{$contact->id}}">
                                    <td class="text-center">{{$contact->country}}</td>
                                    <td class="text-center">{{$contact->phone}}</td>
                                    <td class="text-center">
                                        <div style="text-align: center;">
                                            <button class=" btn  btn-sm btn-dark btn-rounded editContact mr-2"><i class="icon-pencil7 mr-2"></i>Edit</button>
                                            <button class="btn btn-sm btn-primary deleteContact "><i class="icon-bin mr-2"></i> Delete</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /content area -->
            <!-- Edit Modal -->
            <div id="editContactModal" class="modal fade" tabindex="-1">

                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title">Edit modal</h5>
                            <input type="hidden" class="edit_url">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="row">
                                <div class="card-body ">
                                    <form>
                                        {{csrf_field()}}

                                        <ul class="nav nav-justified nav-tabs mb-0">
                                            <li class="nav-item"><a href="#borderedd-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                            @foreach($languages as $language)
                                                <li class="nav-item"><a href="#tabb_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                            @endforeach
                                        </ul>

                                        <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                            <div class="tab-pane fade show active m-3" id="borderedd-tab1">
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold">Phone</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" name="phone"  id="phone" class="form-control ">
                                                    </div>
                                                </div>
                                            </div>

                                            @foreach($languages as $key=>$language)
                                                <input type="hidden" name="langs_id[]" class="table-input" value="{{$language->id}}">
                                                <div class="tab-pane fade m-3 tab{{$key}}" id="tabb_{{$language->id}}">
                                                    <div class="form-group row in" id="country">
                                                        <label class="col-lg-2 col-form-label font-weight-semibold" > Country ({{$language->code}})</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="country[]" class="form-control country" >
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="text-right mt-3">
                                            <a href="#" type="button" class="btn bg-warning updateContact">Save changes</a>
                                        </div>
                                    </form>
                                </div>


                                <!-- /bordered tab content -->

                            </div>


                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
@endsection
@section('js')
    <script src="{{asset('admin/js/contact.js')}}"></script>
    <script>

        $( document ).ready(function() {

            // $("#phone").inputmask({
            //     mask: '999 999 9999',
            //     placeholder: ' ',
            //     showMaskOnHover: false,
            //     showMaskOnFocus: false,
            //     onBeforePaste: function (pastedValue, opts) {
            //         var processedValue = pastedValue;
            //
            //         return processedValue;
            //     }
            // });
        });

    </script>

@endsection
