@extends('layouts.admin')
@section('content')
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
      @include('admin.partials.sidebar')

        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span></h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>

                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content pt-0">
                <!-- Main charts -->
                <div class="row">
                </div>
                <!-- /main charts -->
                <!-- Dashboard content -->
                <div class="row">
                    <div class="col-xl-12">
                        <!-- Marketing campaigns -->
                        <div class="card">
                            <div class="card-header header-elements-sm-inline">
                                <h6 class="card-title">...</h6>
                            </div>
                        </div>
                        <!-- /marketing campaigns -->
                    </div>
                </div>
                <!-- /dashboard content -->
            </div>
            <!-- /content area -->


        </div>
        <!-- /main content -->
    </div>
@endsection

