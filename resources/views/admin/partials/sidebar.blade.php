<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                    <a href="#">
                        <img src="http://turan_blog.test/user/img/logo/edited-logo.png" class="img-fluid mb-2" width="180" height="60" alt="">
                    </a>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <li class="nav-item"><a href="../../../RTL/default/full/index.html" class="nav-link">
                        <i class="mb-2"></i> <span></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.about')}}" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
							About Us
						</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.biography')}}" class="nav-link ">
                        <i class="icon-home4"></i>
                        <span>
							Biography
						</span>
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.sliders')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    Sliders
                                </span>
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.professionals')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    Professionals
                                </span>
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.projects')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    Projects
                                </span>
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.news')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    News
                                </span>
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.partners')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    Partners
                                </span>
                    </a>
                </li>
{{--                <li class="nav-item"><a href="{{route('admin.statistics')}}" class="nav-link">--}}
{{--                        <i class="icon-width"></i>--}}
{{--                        <span>--}}
{{--                                    Statistic--}}
{{--                                </span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="nav-item"><a href="{{route('admin.comments')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    Comments
                                </span>
                    </a>
                </li>
                <li class="nav-item"><a href="{{route('admin.contacts')}}" class="nav-link">
                        <i class="icon-width"></i>
                        <span>
                                    Contacts
                                </span>
                    </a>
                </li>



            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->

</div>
