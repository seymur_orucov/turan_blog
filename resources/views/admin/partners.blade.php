@extends('layouts.admin')
@section('content')
<!-- Page content -->
<div class="page-content">

    @include('admin.partials.sidebar')

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Partners</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="datatable_basic.html" class="breadcrumb-item">Partners</a>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    <div class="breadcrumb justify-content-center">
                        <a href="#" class="breadcrumb-elements-item">
                            <i class="icon-comment-discussion mr-2"></i>
                            Support
                        </a>

                        <div class="breadcrumb-elements-item dropdown p-0">
                            <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear mr-2"></i>
                                Settings
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                                <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                                <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="card">
                <div class="card-header bg-white header-elements-inline">

                    <h5 class="card-title">Partner List</h5>
                    <div class="header-elements">
                        <a  class="btn  btn-sm btn-dark" data-toggle="collapse"
                            href="#section-collapsed" >Add Partner </a>
                    </div>

                </div>
                <div class="collapse" id="section-collapsed">
                    <div class="col-md-10 offset-md-1 mt-2">

                        <div class="card-body m-2">
                            <form  enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label font-weight-semibold">Image upload:</label>
                                    <div class="col-lg-10 d-flex justify-content-end">
                                        <div class="form-check form-check-switch form-check-switch-left">
                                            <label class="form-check-label d-flex align-items-center">
                                                <input type="hidden"  id="other"  name="switch" value="1">
                                                <input type="checkbox" id="switch" data-on-text="On" data-off-text="Off" class="form-check-input-switch" data-size="small" checked >
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <input type="file" class="file-input-ajax"  name="image" id="images" data-fouc>
                                        <img src="" id="previews" alt="" style="display:none;">
                                    </div>
                                </div>


                                <div class="text-right mt-3">
                                    <button type="button" class="btn btn-success savePartner"> <span> Create  </span><i class="icon-paperplane ml-2"></i></button>
                                </div>

                            </form>
                        </div>

                    </div>
                    <hr>
                </div>
                <div class="container-fluid mt-3">
                    <table class="table datatable-partners" data-route="{{route('admin.partners')}}">
                        <thead>
                        <tr>
                            <th class="text-center" >IMAGE</th>
                            <th class="text-center">ACTIONS</th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($partners as $partner)
                            <tr data-id="{{$partner->id}}">

                                <td class="text-center">
                                    <a href="{{url('/')}}/user/img/client/{{$partner->image}}" data-popup="lightbox">
                                        <img src="{{url('/')}}/user/img/client/{{$partner->image}}" alt="" class="img-preview rounded" style="max-weight:100px;">
                                    </a>
                                </td>

                                <td class="text-center">
                                    <div style="text-align: center;">
                                        <button class=" btn  btn-sm btn-dark btn-rounded editPartner mr-2"><i class="icon-pencil7 mr-2"></i>Edit</button>
                                        <button class="btn btn-sm btn-primary deletePartner "><i class="icon-bin mr-2"></i> Delete</button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <!-- /content area -->

        <!-- Edit Modal -->
        <div id="editModal" class="modal fade" tabindex="-1">

            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title">Edit modal</h5>
                        <input type="hidden" class="edit_url">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row">

                            <div class="card-body ">
                                <form>
                                    {{csrf_field()}}

                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label font-weight-semibold">Image upload:</label>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-lg-10">
                                            <input type="file" class="file-input-ajax"  name="image" id="image" data-fouc>
                                            <img src="" id="preview" alt="" style="display:none;">
                                        </div>
                                    </div>

                                    <div class="text-right mt-3">
                                        <a href="#" type="button" class="btn bg-warning updatePartner">Save changes</a>
                                    </div>
                                </form>
                            </div>


                            <!-- /bordered tab content -->

                        </div>


                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
@endsection

@section('js')
    <script src="{{asset('admin/js/partner.js')}}"></script>
@endsection

