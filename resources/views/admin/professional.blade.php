@extends('layouts.admin')
@section('content')
    <!-- Page content -->
    <div class="page-content">

    @include('admin.partials.sidebar')

    <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Professional</h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>


                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="datatable_basic.html" class="breadcrumb-item">Professional</a>
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>


                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                <div class="card">
                    <div class="card-header bg-white header-elements-inline">

                        <h5 class="card-title">Professional List</h5>
                        <div class="header-elements">
                            <a  class="btn  btn-sm btn-dark" data-toggle="collapse"
                                href="#section-collapsed" >Add Professional </a>
                        </div>

                    </div>
                    <div class="collapse" id="section-collapsed">
                        <div class="col-md-10 offset-md-1 mt-2">

                            <div class="card-body m-2">
                                <form>
                                    {{csrf_field()}}
                                    <ul class="nav nav-justified nav-tabs mb-0">
                                        <li class="nav-item"><a href="#bordered-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                        @foreach($languages as $language)
                                            <li class="nav-item"><a href="#tab_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                        <div class="tab-pane fade show active m-3" id="bordered-tab1">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label font-weight-semibold">Icon </label>
                                                <div class="col-lg-10">
                                                    <select data-placeholder="Select icons ..." class="form-control select-icons"  name="icon" data-fouc>
                                                        <option></option>
                                                        @foreach($icons as $icon)
                                                            <option value="{{$icon}}.png" data-icon="{{$icon}}" >{{$icon}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($languages as $language)
                                            <input type="hidden" name="lang_id[]" class="table-input" value="{{$language->id}}">
                                            <div class="tab-pane fade m-3" id="tab_{{$language->id}}">
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold" >Title ({{$language->code}})</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" name="titles[]" placeholder="Title" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold">Description ({{$language->code}})</label>
                                                    <div class="col-lg-10 desc">
                                                        <input type="text" name="descriptions[]" placeholder="Description" class="summernote form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="text-right mt-3">
                                        <button type="button" class="btn btn-success saveProfessional"> <span> Create  </span><i class="icon-paperplane ml-2"></i></button>
                                    </div>

                                </form>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="container-fluid mt-3">
                        <table class="table datatable-professionals" data-edit="{{$edit_route}}" data-save="{{$save_route}}" data-route="{{route('admin.professionals.store')}}">
                            <thead>
                            <tr>
                                 <th>Title</th>
                                 <th>Description </th>
                                 <th>Icon</th>
                                 <th>Actions</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($professionals as $professional)
                                <tr data-id="{{$professional->id}}">
                                    <td>{{substr($professional->title,0,50)}}{{strlen($professional->title)>50 ? "..." : ""}}</td>
                                    <td> {{substr($professional->description,0,50)}}{{strlen($professional->description)>50 ? "..." : ""}} </td>
                                    <td>{{$professional->icon}}</td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="list-icons-item dropdown">
                                                <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item editProfessional" data-toggle="modal" ><i class="icon-pencil7"></i> Edit </a>
                                                    <a href="#" class="dropdown-item deleteProfessional " data-toggle="modal" ><i class="icon-bin"></i> Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
            <!-- /content area -->

            <!-- Edit Modal -->
            <div id="editModal" class="modal fade" tabindex="-1">

                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title">Edit modal</h5>
                            <input type="hidden" class="edit_url">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="row">

                                <div class="card-body ">
                                    <form>
                                        {{csrf_field()}}

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-semibold">Image upload:</label>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-10">
                                                <input type="file" class="file-input-ajax"  name="image" id="image" data-fouc>
                                                <img src="" id="preview" alt="" style="display:none;">
                                            </div>
                                        </div>

                                        <div class="text-right mt-3">
                                            <a href="#" type="button" class="btn bg-warning updateProfessional">Save changes</a>
                                        </div>
                                    </form>
                                </div>


                                <!-- /bordered tab content -->

                            </div>


                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /main content -->

        <!-- Edit Modal -->
        <div id="editProfessionalModal" class="modal fade" tabindex="-1">

            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title">Edit Modal</h5>
                        <input type="hidden" class="edit_url">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-10 offset-md-1 ">
                                <div class="card-body m-2">
                                    <form>
                                        {{csrf_field()}}
                                        <ul class="nav nav-justified nav-tabs mb-0">
                                            <li class="nav-item"><a href="#borderedd-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                            @foreach($languages as $language)
                                                <li class="nav-item"><a href="#tabb_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                            <div class="tab-pane fade show active m-3" id="borderedd-tab1">

                                                <div class="form-group row in" id="icon">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold">Icon </label>
                                                    <div class="col-lg-10">
                                                        <select class="main-icons" name="main-icons">
                                                            <option></option>
                                                            @foreach($icons as $icon)
                                                                <option value="{{$icon}}.png" data-icon="{{$icon}}" >{{$icon}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            @foreach($languages as $key=>$language)
                                                <input type="hidden" name="langs_id[]" class="table-input" value="{{$language->id}}">
                                                <div class="tab-pane fade m-3 tab{{$key}}" id="tabb_{{$language->id}}">

                                                    <div class="form-group row in" id="title">
                                                        <label class="col-lg-2 col-form-label font-weight-semibold" >Title ({{$language->code}})</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="title[]" placeholder="Title" class="form-control title" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group row in" id="description">
                                                        <label class="col-lg-2 col-form-label font-weight-semibold">Description ({{$language->code}})</label>
                                                        <div class="col-lg-10 desc" >
                                                            <input type="text" name="description[]" placeholder="Description" class="summernote form-control ">
                                                        </div>
                                                    </div>

                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="text-right mt-3">
                                            <a href="#" type="button" class="btn bg-warning updateProfessional">Save changes</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /bordered tab content -->

                        </div>

                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /page content -->
@endsection

@section('js')
    <script src="{{asset('admin/js/professional.js')}}"></script>
@endsection

