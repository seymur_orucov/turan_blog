@extends('layouts.admin')
@section('content')
    <!-- Page content -->
    <div class="page-content">

    @include('admin.partials.sidebar')

    <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Projects</h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="{{url('/')}}/admin/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="{{route('admin.projects')}}" class="breadcrumb-item">Projects</a>
                            <a href="{{route('admin.projects.open',$project->id)}}" class="breadcrumb-item">{{$project->title}}</a>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <div class="card">
                    <div class="card-header bg-white header-elements-inline">
                        <h5 class="card-title">{{$project->title}}</h5>
                    </div>
                    <div class="col-md-10 offset-md-1 mt-2">
                        <div class="card-body m-2">
                            <form  action="{{route('admin.projects.open',$project->id)}}" method="POST">
                                {{csrf_field()}}
                                <ul class="nav nav-justified nav-tabs mb-0">
                                    @foreach($languages as $language)
                                        <li class="nav-item"><a href="#tab_{{$language->id}}" class="nav-link rounded-top  @if($loop->first) active @endif" data-toggle="tab">{{$language->name}}</a></li>
                                    @endforeach
                                </ul>
                                <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                    @foreach($languages as$key=>$language)
                                        <input type="hidden" name="lang_id[]" class="table-input" value="{{$language->id}}">
                                        <div class="tab-pane fade @if($loop->first) show active @endif m-3 tab{{$key}}" id="tab_{{$language->id}}" >
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label font-weight-semibold">Text ({{$language->code}})</label>
                                                <div class="col-lg-10 desc">
                                                    <input type="text" name="descriptions[]"  placeholder="Description" class="summernote form-control">
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="text-right mt-3">
                                    <input type="hidden" class="description_data" value="{{$description}}">
                                    <button type="button" class="btn btn-success saveOneProject" data-route="{{route('admin.projects.open',$project->id)}}"> <span> Save  </span><i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
@endsection

@section('js')
    <script>

        $( document ).ready(function() {
            $('.summernote').summernote();
            $(".desc .note-editable").addClass('description-editor');

            var description=$('.description_data').val();

            console.log(description)
            $.each(JSON.parse(description),function (key,value) {
                $(`.tab${key} .description-editor`).html(value);
            })

            $(".saveOneProject").on("click",function(){

                var url = $(this).data('route');

                var description=$(this).closest('form').find('.description-editor').map(function(item){
                    if($(this).html() !== "<br>"){
                        return $(this).html();
                    }else{
                        return $(this).text();
                    }
                }).get();

                var lang_id=$('input[name="lang_id[]"]').map(function(){
                    return $(this).val();
                }).get();

                $.ajax({
                    url: url,
                    method: "POST",
                    data:{
                        lang_id,
                        description,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response) {
                        console.log(response);
                        if(response.length >0){
                            swal("Save successfully !","You clicked the button!",
                                "success" ).then(function(){
                                    location.reload();
                                }
                            );
                        }
                    }
                });
            });
        });

    </script>

@endsection
