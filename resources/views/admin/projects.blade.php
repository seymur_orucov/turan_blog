@extends('layouts.admin')
@section('content')
    <!-- Page content -->
    <div class="page-content">

    @include('admin.partials.sidebar')

    <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Projects</h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="{{url('/')}}/admin/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="{{route('admin.projects')}}" class="breadcrumb-item">Projects</a>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <div class="card">
                    <div class="card-header bg-white header-elements-inline">

                        <h5 class="card-title">Projects List</h5>
                        <div class="header-elements">
                            <a  class="btn  btn-sm btn-dark" data-toggle="collapse"
                                href="#section-collapsed" >Add Project </a>
                        </div>

                    </div>
                    <div class="collapse" id="section-collapsed">
                        <div class="col-md-10 offset-md-1 mt-2">

                            <div class="card-body m-2">
                                <form  enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <ul class="nav nav-justified nav-tabs mb-0">
                                        <li class="nav-item"><a href="#bordered-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                        @foreach($languages as $language)
                                            <li class="nav-item"><a href="#tab_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                        <div class="tab-pane fade show active m-3" id="bordered-tab1">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label font-weight-semibold">Image upload:</label>
                                                <div class="col-lg-10 d-flex justify-content-end">
                                                    <div class="form-check form-check-switch form-check-switch-left">
                                                        <label class="form-check-label d-flex align-items-center">
                                                            <input type="hidden"  id="other"  name="switch" value="1">
                                                            <input type="checkbox" id="switch" data-on-text="On" data-off-text="Off" class="form-check-input-switch" data-size="small" checked >
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <input type="file" class="file-input-ajax"  name="image" id="images" data-fouc>
                                                    <img src="" id="previews" alt="" style="display:none;">
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($languages as $language)
                                            <input type="hidden" name="lang_id[]" class="table-input" value="{{$language->id}}">
                                            <div class="tab-pane fade m-3" id="tab_{{$language->id}}">
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold" >Title ({{$language->code}})</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" name="titles[]" placeholder="Title" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label font-weight-semibold">Description ({{$language->code}})</label>
                                                    <div class="col-lg-10 desc">
                                                        <input type="text" name="descriptions[]" placeholder="Description" class="summernote form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="text-right mt-3">
                                        <button type="button" class="btn btn-success saveProject"> <span> Create  </span><i class="icon-paperplane ml-2"></i></button>
                                    </div>

                                </form>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="container-fluid mt-3">
                        <table class="table datatable-projects" data-route="{{route('admin.projects')}}">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Actions</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr data-id="{{$project->id}}">
                                    <td><a href="{{route('admin.projects.open',$project->id)}}"> {{substr($project->title,0,50)}}{{strlen($project->title)>50 ? "..." : ""}}</a></td>
                                    <td> {{substr($project->description,0,50)}}{{strlen($project->description)>50 ? "..." : ""}} </td>
                                    <td class="text-center">
                                        <a href="{{url('/')}}/user/img/blog/{{$project->image}}" data-popup="lightbox">
                                            <img src="{{url('/')}}/user/img/blog/{{$project->image}}" alt="" class="img-preview rounded" style="max-weight:100px;">
                                        </a>
                                    </td>

                                    <td class="text-center">
                                        <div style="text-align: center;">
                                            <button class=" btn  btn-sm btn-dark btn-rounded editProject mr-2"><i class="icon-pencil7 mr-2"></i>Edit</button>
                                            <button class="btn btn-sm btn-primary deleteProject "><i class="icon-bin mr-2"></i> Delete</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <!-- /content area -->

            <!-- Edit Modal -->
            <div id="editProjectModal" class="modal fade" tabindex="-1">

                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title">Edit modal</h5>
                            <input type="hidden" class="edit_url">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="row">

                                <div class="card-body ">
                                    <form>
                                        {{csrf_field()}}


                                        <ul class="nav nav-justified nav-tabs mb-0">
                                            <li class="nav-item"><a href="#borderedd-tab1" class="nav-link rounded-top active" data-toggle="tab">GENERAL</a></li>
                                            @foreach($languages as $language)
                                                <li class="nav-item"><a href="#tabb_{{$language->id}}" class="nav-link rounded-top" data-toggle="tab">{{$language->name}}</a></li>
                                            @endforeach
                                        </ul>

                                        <div class="tab-content card card-body border-top-0 rounded-top-0 mb-0">
                                            <div class="tab-pane fade show active m-3" id="borderedd-tab1">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label font-weight-semibold">Image upload:</label>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-10">
                                                        <input type="file" class="file-input-ajax"  name="image" id="image" data-fouc>
                                                        <img src="" id="preview" alt="" style="display:none;">
                                                    </div>
                                                </div>
                                            </div>

                                            @foreach($languages as $key=>$language)
                                                <input type="hidden" name="langs_id[]" class="table-input" value="{{$language->id}}">
                                                <div class="tab-pane fade m-3 tab{{$key}}" id="tabb_{{$language->id}}">

                                                    <div class="form-group row in" id="title">
                                                        <label class="col-lg-2 col-form-label font-weight-semibold" >Title ({{$language->code}})</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="title[]" placeholder="Title" class="form-control title" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group row in" id="description">
                                                        <label class="col-lg-2 col-form-label font-weight-semibold">Description ({{$language->code}})</label>
                                                        <div class="col-lg-10 desc" >
                                                            <input type="text" name="description[]" placeholder="Description" class="summernote form-control ">
                                                        </div>
                                                    </div>

                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="text-right mt-3">
                                            <a href="#" type="button" class="btn bg-warning updateProject">Save changes</a>
                                        </div>
                                    </form>
                                </div>


                                <!-- /bordered tab content -->

                            </div>


                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
@endsection

@section('js')
    <script src="{{asset('admin/js/project.js')}}"></script>
@endsection

