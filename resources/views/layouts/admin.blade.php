<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Admin Panel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('admin/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('admin/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('admin/css/layout.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('admin/css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('admin/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/global_assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
    <style>
        .page-header-light {
            background-color: #eeeded !important;
        }
    </style>
	<!-- /global stylesheets -->



</head>

<body>

	@include('admin.partials.header')
	@yield('content')
	@include('admin.partials.footer')


	</div>
	<!-- /main content -->

</div>
<!-- /page content -->


<!-- Theme JS files -->

    <script src="{{asset('admin/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>

    <script src="{{asset('admin/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/inputs/maxlength.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/uploader_bootstrap.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/notifications/sweet_alert.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/wizards/steps.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/notifications/bootbox.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/components_modals.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/inputs/inputmask.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/extensions/cookie.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/form_wizard.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/media/fancybox.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/gallery_library.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/extensions/jquery_ui/touch.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/components_collapsible.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/form_select2.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/inputs/autosize.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/inputs/formatter.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/inputs/passy.js')}}"></script>
    <script src="{{asset('admin/js/app.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/inputs/touchspin.min.js')}}"></script>

    <script src="{{asset('admin/js/custom.js')}}"></script>
{{--    <script src="{{asset('admin/global_assets/js/demo_pages/form_validation.js')}}"></script>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
{{--    <script src="{{asset('admin/js/sweet_alert.js')}}"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script src="{{asset('admin/js/app.js')}}"></script>
    <script src="{{asset('admin/js/sweet_alert.js')}}"></script>

@yield('js')
</body>
</html>

