<!doctype html>
<html class="no-js" lang="zxx">



<head>
    <!-- Required Meta Tags Start -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Turan Özbahceci</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Required Meta Tags End -->

    <!-- Favicon Tags Start -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('user/img/logo/ozbahceci-logo.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('user/img/logo/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('user/img/logo/favicon-16x16.png')}}">
    <!--    <link rel="manifest" href="{{asset('user/favicons/site.html')}}">-->
    <!--    <link rel="mask-icon" href="{{asset('user/favicons/safari-pinned-tab.svg')}}" color="#5bbad5">-->
    <!--    <link rel="shortcut icon" href="{{asset('user/img/logo/favicon.ico')}}">-->
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="msapplication-config" content="{{asset('user/favicons/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon Tags End -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('user/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/progressbar.css')}}">
    @yield('css')
</head>

<body>


<!-- |==========================================| -->
{{--<!-- |=====|| Preloader start ||===============| -->--}}
{{--<div class="preloader">--}}
{{--    <div class="lds-ellipsis">--}}
{{--        <div></div>--}}
{{--        <div></div>--}}
{{--        <div></div>--}}
{{--        <div></div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- |=====|| Preloader end ||=================| -->
<!-- |==========================================| -->


<!-- |==========================================| -->
<!-- |=====|| Header Start ||===============| -->
<header class="header">
    <!-- Header Menu Start -->
    <div class="header__menu">
        <div class="header__menu-wrapper">
            <div class="container-fluid">
                <div class="header__menu-outer">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- logo -->
                            <div class="header__logo f-left">
                                <a href="{{route('index')}}"><img src="{{asset('user/img/logo/edited-logo.png')}}" alt=""></a>
                            </div>
                            <!-- nav -->
                            <div class="main-menu text-center f-left">
                                <nav id="mobile-menu">
                                    <ul>
                                        <li><a href="{{route('index')}}">Ana Səhifə</a></li>
                                        <li><a href="{{route('biography')}}">Bioqrafiya</a></li>
                                        <li><a href="{{route('projects')}}">Layihələr</a></li>
                                        <li><a href="{{route('news')}}">Xəbərlər</a></li>
                                        <li><a href="{{route('comments',['id'=>$data_id])}}">Rəylər</a></li>
                                        <li><a href="{{route('contact')}}">Əlaqə</a></li>
                                    </ul>
                                </nav>
                            </div>

                            <div class="language-row f-left">
                                <div class="language-row-inner">
                                    <ul class="language-select">
                                        <li class="active"  data-lang="v" id="0">
                                         <a href="#">
                                            @lang('lang.choice')
                                         </a>

                                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                            @if (LaravelLocalization::getCurrentLocale() !=$localeCode)
                                                <li data-lang="{{ $localeCode }}" >
                                                    <a hreflang="{{ $localeCode }}"  href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                        @lang('lang.'.$localeCode)
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <!-- search and shopping-cart -->
                            <div class="header__side-nav text-right f-right d-none d-lg-block">
                                <ul>
                                    <li class="search_box_container">
                                        <button class="search_btn button"><span class="ti-search"></span></button>
                                        <div class="search_form">
                                            <form action="{{route('search')}}" method="POST">
                                                @csrf
                                                <input type="text" placeholder="Axtar" name="searchResult">
                                                <button class="button searchData" data-url="{{route('search')}}" ><span class="ti-search"></span></button>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="mobile-menu"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Menu End -->
</header>
<!-- |=====|| Header End ||=================| -->
<!-- |==========================================| -->

@yield('contain')

<!-- |=====|| Footer Start ||===============| -->
<footer class="footer1">
    <div class="footer1__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer1__top--wrapper">

                        <nav class="footer1__nav">
                            <ul>
                                <li><a href="{{route('index')}}">Ana Səhifə</a></li>
                                <li><a href="{{route('biography')}}">Bioqrafiya</a></li>
                                <li><a href="{{route('projects')}}">Layihələr</a></li>
                                <li><a href="{{route('news')}}">Xəbərlər</a></li>
                                <li><a href="{{route('comments',['id'=>$data_id])}}">Rəylər</a></li>
                                <li><a href="{{route('contact')}}">Əlaqə</a></li>
                            </ul>
                        </nav>
                        <div class="footer1__social">
                            <ul>
                                <li><a href="https://www.facebook.com/turan.ozbahceci"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCmSeMwPTRlAaY4NN2Bdl-6w/"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="https://www.linkedin.com/in/turan-ozbahceci-793213139/"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="https://www.instagram.com/turan_ozbahceci/?igshid=841q91mj0plo"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer1__bottom text-center">
        <p class="m-0">© 2020 Müəllif hüquqları qorunur.</p>
    </div>
</footer>
<!-- |=====|| footer End ||=================| -->
<!-- |==========================================| -->


<!-- |==========================================| -->
<!-- |=====|| ScrollToTop Start ||===============| -->
<a href="#" class="scrollToTop home3">
    <i class="fas fa-level-up-alt"></i>
</a>
<!-- |=====|| ScrollToTop End ||=================| -->
<!-- |==========================================| -->



<!-- |==========================================| -->
<!-- |=====|| All js here ||===============| -->
<script src="{{asset('user/js/swiper.min.js')}}"></script>
<script src="{{asset('user/js/second-swiper.js')}}"></script>
<script src="{{asset('user/js/vendor/modernizr-3.6.0.min.js')}}"></script>
<script src="{{asset('user/js/vendor/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('user/js/popper.min.js')}}"></script>
<script src="{{asset('user/js/bootstrap-v4.3.1.min.js')}}"></script>
<script src="{{asset('user/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('user/js/plugins.js')}}"></script>
<script src="{{asset('user/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('user/js/wow.min.js')}}"></script>
<script src="{{asset('user/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('user/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('user/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('user/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('user/js/slick.min.js')}}"></script>
<script src="{{asset('user/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('user/js/jquery.meanmenu.min.js')}}"></script>
<script src="{{asset('user/js/selectbox.js')}}"></script>
<!-- Google Map api -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="{{asset('user/js/map-script.js')}}"></script>
<!-- Ajax Contact js -->
<script src="{{asset('user/js/ajax-contact.js')}}"></script>
<!-- Main js -->
<script src="{{asset('user/js/main.js')}}"></script>
@yield('js')
<!-- |=====|| All js End ||=================| -->
<!-- |==========================================| -->
</body>


</html>
