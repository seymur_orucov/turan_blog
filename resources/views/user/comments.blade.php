@extends('layouts.master')
@section('contain')
    <main>
        <!-- |==========================================| -->
        <!-- |=====|| Page Title Start ||===============| -->
        <section class="page_title other_pages page_title__bg-13">
            <div class="page_title__padding">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="page_title__content text-center">
                                <h1>Rəylər</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| Page Title End ||=================| -->
        <!-- |==========================================| -->


        <!-- |==========================================| -->
        <!-- |=====|| Blog Single Start ||===============| -->
        <section class="blog_single1">
            <div class="content_box_50_70">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="blog_single1__wrapper ">
                               {!! $text !!}
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="side_bar2 pt-70">
                                <div class="side_bar2__widget mb-45">
                                    <h3>Digər Rəylər</h3>
                                    @foreach($comments as $comment)
                                        <div class="side_bar2__widget--post">
                                            <div class="side_bar2__post-content">
                                                <h4><a href="{{route('comments',$comment->id)}}">{{$comment->title}}</a></h4>
                                                <span><i class="ti-calendar"></i> {{date('d M, Y',strtotime($comment->created_at))}}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="side_bar2__widget mb-45">
                                <div class="vote">

                                </div>
                            </div>
                            <input type="hidden" class="url" data-vote-url="{{route('vote_send')}}">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====||  End ||=================| -->
        <!-- |==========================================| -->
    </main>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            // Check user vote
            checkVote();
        });

        // Check user vote
        function checkVote(checkedVote=null){
            var url=$(".url").data('vote-url');

            $.ajax({
                url:url,
                method: 'POST',
                data: {vote: 1, checkedVote:checkedVote, '_token': $('meta[name="csrf-token"]').attr('content')},
                success: function(response){
                    $('.vote').html(response);
                }
            });
        }

        // Save user poll
        // function saveVote(){
        $(document).on('click', '.saveVote', function(e) {
            e.preventDefault();
            // var checkedVote = $(".vote input[name='vote']:checked").val();
            var checkedVote=$(".vote input[name='vote']:checked").map(function(){
                return $(this).val();
            }).get();

            if(checkedVote =="all"){
                var checkedVote=$(".vote input[name='vote']").map(function(){
                    if($(this).val() != "all"){
                        return $(this).val();
                    }

                }).get();
            }

            console.log(checkedVote);

            var url=$(".url").data('vote-url');

            if(checkedVote != undefined){
                $.ajax({
                    url:url,
                    method: 'POST',
                    data: {vote: 2,vote_option: checkedVote, '_token': $('meta[name="csrf-token"]').attr('content')},
                    success: function(response){
                        if(response == 1){
                            checkVote(checkedVote);
                        }
                    }
                });
            }
        });
    </script>
@endsection

