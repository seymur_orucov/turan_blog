@extends('layouts.master')
@section('contain')
    <main>
        <!-- |=====|| Contact Start ||===============| -->
        <div class="contact_page1 ">
            <div class="content_box_120_70 pt-100">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="contact_page1__title mb-15">
                                <div class="sub__title mb-25">
                                    <h3>Bizimlə əlaqə saxla!</h3>
                                </div>
                                <div class="title3">
                                    <h2>Bizi tapmağın asan yolu</h2>
                                </div>
                            </div>
                            <div class="contact_page1__content mb-50">
                                <div class="row">
                                    @foreach($contacts as $contact)
                                        <div class="col-sm-6 mt-40">
                                            <h4>{{$contact->country}}</h4>
                                            <div class="contact_page1__item mb-25">
                                                <div class="contact_page1__item--name">
                                                    <i class="ti-mobile"></i>
                                                    <h5>Əlaqə telefonu</h5>
                                                </div>
                                                <p>{{$contact->phone}}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact_page1__form mb-50">
                                <h3>Əlaqə Saxlayın</h3>

                                @if (session('status'))
                                    <div class="alert alert-success">
                                        <span class="span"> {{ session('status')}}</span>
                                    </div>
                                @endif

                                <form action="{{route('contact.send_mail')}}" method="POST">
                                    @csrf
                                    <div class="row mb-20">
                                        <div class="col-xl-12">
                                            <input class="form-control" type="text" name="name" placeholder="Adınız"
                                                   value="{{old('name')}}" required>

                                            @if($errors->has('name'))
                                                <div class="error"><span>{{ $errors->first('name') }}</span></div>
                                            @endif
                                        </div>
                                        <div class="col-xl-12">
                                            <input class="form-control" type="email" name="email"
                                                placeholder="Email Ünvanınız" value="{{old('email')}}" required>

                                            @if($errors->has('email'))
                                                <div class="error"><span>{{ $errors->first('email') }}</span></div>
                                            @endif
                                        </div>
                                        <div class="col-xl-12">
                                            <input class="form-control" type="text" name="subject"
                                                placeholder="Mövzu" value="{{old('subject')}}" required>

                                            @if($errors->has('subject'))
                                                <div class="error"><span>{{ $errors->first('subject') }}</span></div>
                                            @endif
                                        </div>
                                        <div class="col-xl-12">
                                            <textarea class="form-control" name="message"
                                                placeholder="Sizə necə kömək edə bilərik?" cols="30" rows="7"
                                                      value="{{old('message')}}" required></textarea>

                                            @if($errors->has('message'))
                                                <div class="error"><span>{{ $errors->first('message') }}</span></div>
                                            @endif
                                            <button type="submit" class="btn1">Mesaj Göndər</button>
                                        </div>
                                    </div>
{{--                                    <p class="form-message"></p>--}}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- |=====|| Contact End ||=================| -->
    </main>
@endsection
