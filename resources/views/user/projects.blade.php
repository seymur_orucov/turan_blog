@extends('layouts.master')
@section('css')
    <style>
        .page-item{
            display:inline-block;
        }
        .pagination-nav nav{
            margin:0 auto;
        }
        .page-item.active .page-link{
            background:black !important;
            border-color: black;
            color:white !important;
        }
        .page-link{
            color:black !important;
        }

    </style>
@endsection
@section('contain')
    <main>
        <!-- |==========================================| -->
        <!-- |=====|| Page Title Start ||===============| -->
        <section class="page_title page_title__bg-12">
            <div class="page_title__padding">
                <div class="page_title__content text-center">
                    <h1>Layihələr</h1>
                </div>
            </div>
        </section>
        <!-- |=====|| Page Title End ||=================| -->
        <!-- |==========================================| -->

        <!-- |==========================================| -->
        <!-- |=====|| Blog Start ||===============| -->
        <section class="blog_page1">
            <div class="content_box_120_70">
                <div class="container">
                    <div class="row">
                        @foreach($projects as $project)
                            <div class="col-md-6">
                                <div class="blog_page1__item mb-50">
                                    <div class="blog_page1__thumb">
                                        <img src="{{asset('user/img/blog')}}/{{$project->image}}" alt="Blog Image" class="img_390">
                                    </div>
                                    <div class="blog_page1__data mt-35">
                                        <a href="#"><i class="ti-calendar"></i>{{date('d M, m',strtotime($project->created_at))}}</a>
                                    </div>
                                    <div class="blog_page1__content">
                                        <h3>{{$project->title}}</h3>
                                        <p>{{$project->description}}</p>
                                        <a href="{{route('project-description',$project->id)}}" class="site_btn_09">Davam et<i class="ti-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row pagination-nav">
                         {{$projects->links()}}
                    </div>
                </div>
            </div>
        </section>
        <!-- |=====|| Blog End ||=================| -->
        <!-- |==========================================| -->
    </main>
@endsection

