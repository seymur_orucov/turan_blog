@extends('layouts.master')
@section('css')
    <style>
          .search__results{
              font-family:Roboto, sans-serif;
          }
          .page-item{
              display:inline-block;
          }
          .pagination-nav {
              margin:17px;
          }
          .pagination-nav nav{
              margin:0 auto;
          }
          .page-item.active .page-link{
              background:black !important;
              border-color: black;
              color:white !important;
          }
          .page-link{
              color:black !important;
          }

          .pagination-row{
              display:block;
              margin:0 auto;
          }

          #search__pagination li {
              list-style: none;
              font-size: 17px;
              border: 1px solid black;
              padding: 2px 13px;
              display: inline-block;
              margin: 14px 6px;
          }

          #search__pagination li a{
              color: black;
          }

          .search__results{
              font-family:Roboto, sans-serif;
          }

    </style>
@endsection
@section('contain')
    <main>
    <!-- |==========================================| -->
    <!-- |=====|| Page Title Start ||===============| -->
    <section class="page_title">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="search__block">

                        <form class="search__form" novalidate="novalidate">
                            <label>
                                <input type="text" placeholder="Axtarış" name="search_data" autocomplete="off" class="valid" aria-invalid="false" value="{{$search_data}}">
                                <button class="searchHome" data-url="{{route('search-api')}}" data-route="{{route('home')}}">
                                    <i id="search-close" class="ti-search" ></i>
                                </button>
                            </label>
                        </form>
                        <span class="search__result-text">
                            <b>Axtarış nəticələri : {{$count}}</b>
                        </span>
                    </div>
                </div>
{{--                @if($data->column =="news" ||$data->column =="products")"--}}
                <div class="col-12">
                    <div class="search__results">
                        @foreach($result as $data)
                            <div class="search__result">
                                <a class="search__result-title" target="_blank" href="{{url('/')}}/{{LaravelLocalization::getCurrentLocale()}}/{{$data->name}}/{{$data->id}}">{!! $data->title !!} </a>
                                <div class="search__result-description" > @if(isset($data->description)) {{substr($data->description,0,150)}}{{strlen($data->description)>150 ? "..." : ""}} @endif</div>
                            </div>
                        @endforeach
                      </div>
                </div>

            </div>
{{--            <div class="row pagination-nav">--}}
{{--                {{$result->links()}}--}}
{{--            </div>--}}
            <div class="row">
                <div class="pagination-row">
                    <ul id="search__pagination">
                        <li style="display:none;"></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- |=====|| Page Title End ||=================| -->
    <!-- |==========================================| -->


    <!-- |==========================================| -->
    <!-- |=====|| Blog Start ||===============| -->

    <!-- |=====|| Blog End ||=================| -->
    <!-- |==========================================| -->
</main>
@endsection

@section('js')
    <script src="{{asset('user/js/search.js')}}"></script>
@endsection
