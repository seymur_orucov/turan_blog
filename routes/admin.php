<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('admin.dashboard');
});

//
//    Route::get('/login','HomeController@getLogin');
//    Route::post('/login','HomeController@getSignin')->name('signup');
//    Route::post('/signin','HomeController@postSignin')->name('signin');
//
//Route::group([
//    'middleware' => 'role'
//], function () {
    // Partners
    Route::get('partners', 'PartnersController@index')->name('partners');
    Route::get('/partners/{id}', 'PartnersController@index');
    Route::post('/partners', 'PartnersController@store')->name('partners.store');
    Route::get('/partners/edit/{id}', 'PartnersController@edit')->name('partners.edit');
    Route::post('/partners/edit/{id}', 'PartnersController@update');
    Route::get('/partners/delete/{id}', 'PartnersController@destroy')->name('partners.delete');

    // Professionals
    Route::get('professionals', 'ProfessionalController@index')->name('professionals');
    Route::get('/professionals/{id}', 'ProfessionalController@index');
    Route::post('/professionals', 'ProfessionalController@store')->name('professionals.store');
    Route::get('/professionals/edit/{id}', 'ProfessionalController@edit')->name('professionals.edit');
    Route::post('/professionals/edit/{id}', 'ProfessionalController@update');
    Route::get('/professionals/delete/{id}', 'ProfessionalController@destroy')->name('professionals.delete');

    // Sliders
    Route::get('sliders', 'SliderController@index')->name('sliders');
    Route::get('/sliders/{id}', 'SliderController@index');
    Route::post('/sliders', 'SliderController@store')->name('sliders.store');
    Route::get('/sliders/edit/{id}', 'SliderController@edit')->name('sliders.edit');
    Route::post('/sliders/edit/{id}', 'SliderController@update');
    Route::get('/sliders/delete/{id}', 'SliderController@destroy')->name('sliders.delete');

    // Projects
    Route::get('projects', 'ProjectController@index')->name('projects');
    Route::get('/projects/{id}', 'ProjectController@index');
    Route::post('/projects', 'ProjectController@store')->name('projects.store');
    Route::get('/projects/edit/{id}', 'ProjectController@edit')->name('projects.edit');
    Route::post('/projects/edit/{id}', 'ProjectController@update');
    Route::get('/projects/delete/{id}', 'ProjectController@destroy')->name('projects.delete');
    Route::get('/projects-open/{id}', 'ProjectController@open')->name('projects.open');
    Route::post('/projects-open/{id}', 'ProjectController@save')->name('projects.save');


    // News
    Route::get('news', 'NewsController@index')->name('news');
    Route::get('/news/{id}', 'NewsController@index');
    Route::post('/news', 'NewsController@store')->name('news.store');
    Route::get('/news/edit/{id}', 'NewsController@edit')->name('news.edit');
    Route::post('/news/edit/{id}', 'NewsController@update');
    Route::get('/news/delete/{id}', 'NewsController@destroy')->name('news.delete');
    Route::get('/news-open/{id}', 'NewsController@open')->name('news.open');
    Route::post('/news-open/{id}', 'NewsController@save')->name('news.save');

    // Statistics - About - Biography
//    Route::get('statistics', 'StatisticController@index')->name('statistics');
//    Route::post('statistics', 'StatisticController@asve')->name('statistics.save');
    Route::get('about-us', 'AboutController@index')->name('about');
    Route::get('/about-us/edit/{id}', 'AboutController@edit')->name('about.edit');
    Route::post('/about-us/edit/{id}', 'AboutController@update');
    Route::get('biography', 'BiographyController@index')->name('biography');
    Route::post('biography/{id}', 'BiographyController@save')->name('biography.save');

    // Comments
    Route::get('comments', 'CommentController@index')->name('comments');
    Route::get('/comments/{id}', 'CommentController@index');
    Route::post('/comments', 'CommentController@store')->name('comments.store');
    Route::get('/comments/edit/{id}', 'CommentController@edit')->name('comments.edit');
    Route::post('/comments/edit/{id}', 'CommentController@update');
    Route::get('/comments/delete/{id}', 'CommentController@destroy')->name('comments.delete');

    // Contact
    Route::get('contacts', 'ContactController@index')->name('contacts');
    Route::get('/contacts/{id}', 'ContactController@index');
    Route::post('/contacts', 'ContactController@store')->name('contacts.store');
    Route::get('/contacts/edit/{id}', 'ContactController@edit')->name('contacts.edit');
    Route::post('/contacts/edit/{id}', 'ContactController@update');
    Route::get('/contacts/delete/{id}', 'ContactController@destroy')->name('contacts.delete');


//});
