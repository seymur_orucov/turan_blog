<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function() {
    Route::namespace('User')
        ->group(function () {
            // Home
            Route::get('/', 'HomeController@index')->name('index');

            // Biography
            Route::get('/biography', 'BiographyController@index')->name('biography');

            // About
            Route::get('projects', 'ProjectController@index')->name('projects');
            Route::get('project-description/{id}', 'ProjectController@show')->name('project-description');

            // Blog
            Route::get('news', 'NewsController@index')->name('news');
            Route::get('news-description/{id}', 'NewsController@show')->name('news-description');

            // Shop
            Route::get('comments/{id}', 'CommentController@index')->name('comments');
            Route::post('vote', 'CommentController@vote_send')->name('vote_send');

            // Contact
            Route::get('contact', 'ContactController@index')->name('contact');
            Route::post('send_mail', 'ContactController@send_mail')->name('contact.send_mail');

            // Search (one)
            Route::post('/search','SearchController@search')->name('search');
        });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');



});
